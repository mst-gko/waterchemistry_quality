BEGIN;

DROP TABLE IF EXISTS temp_makyn.limited_chemistry_quality CASCADE;

SELECT DISTINCT bwq.boreholeid, bwq.boreholeno, bwq.intakeno, bwq.dgu, bwq.geom, bwq.is_demolished, bwq.quality_waterchemistry, bwq.quality_waterchemistry_complete, bwq.quality_waterchemistry_complete_descr
	INTO temp_makyn.limited_chemistry_quality
	FROM temp_makyn.borehole_waterchemistry_quality bwq 
;

ALTER TABLE temp_makyn.limited_chemistry_quality 
	ADD COLUMN IF NOT EXISTS url TEXT DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS latest_sample_date timestamp(0) NULL,
	ADD COLUMN IF NOT EXISTS no_of_samples NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS ionbalancen NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS nitrat NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS sulfat NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS klorid NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS jern NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS kalium NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS magnesium NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS natrium NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS hydrogencarbonat NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS calcium NUMERIC DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS fohm_layer TEXT DEFAULT NULL,
	ADD COLUMN IF NOT EXISTS fohm_nr NUMERIC DEFAULT NULL

	;

GRANT USAGE ON SCHEMA temp_makyn TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA temp_makyn TO grukosreader; 

-- Create geometry index
	CREATE INDEX limited_chemistry_quality_geom_idx
  		ON temp_makyn.limited_chemistry_quality
			USING GIST (geom);

UPDATE temp_makyn.limited_chemistry_quality AS b
		SET url = concat('http://data.geus.dk/JupiterWWW/borerapport.jsp?borid=', b.boreholeid) 
	;

UPDATE temp_makyn.limited_chemistry_quality AS b
		SET  klorid = alls.kl, nitrat = alls.n, sulfat = alls.s, jern = alls.j, kalium = alls.k, magnesium = alls.m,
			hydrogencarbonat = alls.hc, calcium = alls.c, natrium = alls.na
		FROM
			(SELECT
							sampleid, bwc.dgu, bwc.sampledate AS sd,
							chlorid AS kl,
							natrium AS na,
							nitrat AS n,
							sulfat AS s,
							jern AS j,
							kalium AS k,
							magnesium AS m,
							hydrogencarbonat AS hc,
							calcium AS c
						FROM mstjupiter.inorganic_crosstab
						INNER JOIN temp_makyn.borehole_waterchemistry_quality bwc USING (sampleid)
              			WHERE COALESCE(chlorid, natrium, nitrat, sulfat, kalium, magnesium, hydrogencarbonat, calcium) IS NOT NULL 
              			ORDER BY sd asc NULLS last
						) AS alls
		WHERE alls.dgu = b.dgu
			AND alls.sd <= current_date
	; 

UPDATE temp_makyn.limited_chemistry_quality AS b
        SET latest_sample_date = gw2.sd
        FROM 
         (         
         
             SELECT gw.boreholeno, gw.intakeno, gw.boreholeid, max(gw.sampledate) AS sd
             FROM jupiter.grwchemsample gw
             INNER JOIN jupiter.grwchemanalysis ca ON gw.sampleid = ca.sampleid
             INNER JOIN temp_makyn.limited_chemistry_quality bb ON bb.dgu = concat(gw.boreholeno, '_', COALESCE(gw.intakeno)) 
             INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 
             WHERE gw.sampledate  <= current_date
				AND COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
				, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
			GROUP BY gw.boreholeno, gw.intakeno, gw.boreholeid
            ) AS gw2
        WHERE concat(gw2.boreholeno, '_', COALESCE(gw2.intakeno)) = b.dgu
	;

UPDATE temp_makyn.limited_chemistry_quality AS b
        SET no_of_samples = gw2.ns
        FROM 
         (
             SELECT gw.boreholeno, gw.intakeno, count(DISTINCT gw.sampledate) AS ns
             FROM jupiter.grwchemsample gw
             INNER JOIN jupiter.grwchemanalysis ca ON gw.sampleid = ca.sampleid
             INNER JOIN temp_makyn.limited_chemistry_quality bb ON bb.dgu = concat(gw.boreholeno, '_', COALESCE(gw.intakeno)) 
             INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 
             WHERE sampledate <= current_date
				AND COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
				, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
             GROUP BY gw.boreholeno, gw.intakeno
            ) AS gw2
        WHERE concat(gw2.boreholeno, '_', COALESCE(gw2.intakeno)) = b.dgu
	;	

UPDATE temp_makyn.limited_chemistry_quality AS b 
		SET ionbalancen = cs.ionn
		FROM 
		(
		SELECT  sampleid, bwc.dgu, ionbalance AS ionn, bwc.sampledate AS sd
		FROM mstjupiter.forvitgrad_ionbalance_ionbytning
		INNER JOIN temp_makyn.borehole_waterchemistry_quality bwc USING (sampleid)
		INNER JOIN jupiter.grwchemsample ca USING (sampleid)
		ORDER BY sd asc NULLS last
		) AS cs
				WHERE cs.dgu = b.dgu
					AND COALESCE(b.klorid, b.nitrat, b.sulfat, b.kalium, b.magnesium, b.natrium
				, b.hydrogencarbonat, b.calcium) IS NOT NULL 
				AND cs.sd = b.latest_sample_date
		;
UPDATE temp_makyn.limited_chemistry_quality AS b
        SET fohm_layer = concat(z.layer_num, '_', z.litho), fohm_nr = z.layer_num
        FROM
        (
        SELECT *
        FROM temp_simak.fohm_filter_lith_all
        ) AS z
        WHERE z.boreholeid = b.boreholeid ;
       
    
COMMIT;
