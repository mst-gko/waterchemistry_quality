BEGIN;
	
	DROP TABLE IF EXISTS temp_makyn.borehole_waterchemistry_quality CASCADE;

	CREATE TABLE temp_makyn.borehole_waterchemistry_quality AS
		(
			select distinct	
				gcs.boreholeid, gcs.boreholeno,
				gcs.sampleid, gcs.sampledate, gcs.intakeno, gca.compoundno, 
				cpl.long_text,
				gca.guid AS uuid,
				CASE
					WHEN upper(b.use) = 'S'
						THEN 't'::boolean
					ELSE 'f'::boolean
					END AS is_demolished,
				b.geom
				FROM jupiter.grwchemsample gcs 
				INNER JOIN jupiter.grwchemanalysis gca 
					ON gcs.sampleid = gca.sampleid 
				LEFT JOIN jupiter.compoundlist cpl 
					ON gca.compoundno = cpl.compoundno
				LEFT JOIN jupiter.borehole b 
					ON gcs.boreholeid = b.boreholeid 
			)
	;


	ALTER TABLE temp_makyn.borehole_waterchemistry_quality  ADD CONSTRAINT uuid_unique UNIQUE (uuid);
	
	 ALTER TABLE temp_makyn.borehole_waterchemistry_quality	
		ADD COLUMN IF NOT EXISTS DGU text DEFAULT NULL
   	;

	UPDATE temp_makyn.borehole_waterchemistry_quality AS b 
		SET DGU = concat(b.boreholeno, '_', COALESCE(b.intakeno))
		;

	-- Create geometry index
	CREATE INDEX borehole_waterchemistry_quality_geom_idx
  		ON temp_makyn.borehole_waterchemistry_quality
			USING GIST (geom);

	CREATE INDEX borehole_waterchemistry_quality_borid_idx
  		ON temp_makyn.borehole_waterchemistry_quality
  			USING BTREE (DGU);
		--	USING BTREE (boreholeid);

	CREATE INDEX borehole_waterchemistry_quality_uuid_idx
  		ON temp_makyn.borehole_waterchemistry_quality
			USING BTREE (uuid);
			

    -- Add columns
    ALTER TABLE temp_makyn.borehole_waterchemistry_quality	
    	ADD COLUMN quality_waterchemistry INTEGER DEFAULT 0
   	;
   


-- Qry_chemistrydata_2 -- there is an existing inorganic sample
    UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET quality_waterchemistry = 2
    WHERE b.DGU IN
		(
			  SELECT DISTINCT concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
			  FROM jupiter.grwchemsample gw
			  INNER JOIN jupiter.grwchemanalysis ca ON ca.sampleid = gw.sampleid
              INNER JOIN jupiter.compoundlist cl ON ca.compoundno = cl.compoundno
              			INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 

			  WHERE cl.long_TEXT IN 
			 (
				'Nitrat', 'Sulfat', 'Chlorid', 'Kalium', 'Magnesium', 'Natrium', 'Hydrogencarbonat', 'Calcium'
			)
			  AND COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
											, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
		)
	;


	-- Qry_chemistrydata_3 -- more than 3 samples
	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
	SET quality_waterchemistry = 3
	WHERE quality_waterchemistry >= 2
		AND b.DGU IN
			(
				SELECT DISTINCT concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
				FROM jupiter.grwchemsample gw
				INNER JOIN jupiter.grwchemanalysis ca ON ca.sampleid = gw.sampleid
              INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 
			  								WHERE  COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
											, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
				GROUP BY concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
				HAVING count(distinct EXTRACT(YEAR FROM sampledate)) > 3
			)
	;


	-- Qry_chemistrydata_4 -- sample date younger than 5 years
	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
	SET quality_waterchemistry = 4
	WHERE quality_waterchemistry >= 2
		AND b.DGU IN
			(WITH
               latest_samp AS
                             (SELECT DISTINCT ON (guid_intake)

                                            guid_intake, boreholeno, intakeno, gcs.sampleid, gcs.sampledate
                                            FROM jupiter.grwchemsample gcs
                                            INNER JOIN jupiter.grwchemanalysis ca USING (sampleid)
              								INNER JOIN mstjupiter.inorganic_crosstab ic ON gcs.sampleid = ic.sampleid 
			  								WHERE  COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
											, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
                                            ORDER BY guid_intake, gcs.sampledate DESC NULLS LAST
                             )

				SELECT DISTINCT concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
			    FROM latest_samp gw
			    INNER JOIN jupiter.grwchemanalysis ca ON ca.sampleid = gw.sampleid
              INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 
			  								WHERE  COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
											, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
											
			    AND gw.sampledate > now() - '5 years' :: INTERVAL
			    	AND gw.sampledate <= current_date
			)
	;

    -- Qry_chemistrydata_5 -- multiple samples
	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
	SET quality_waterchemistry = 5
	WHERE quality_waterchemistry > 3
		AND b.DGU IN 
			(
				SELECT DISTINCT concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
				FROM jupiter.grwchemsample gw
				INNER JOIN jupiter.grwchemanalysis ca ON ca.sampleid = gw.sampleid
              INNER JOIN mstjupiter.inorganic_crosstab ic ON gw.sampleid = ic.sampleid 
			  WHERE  COALESCE(ic.chlorid, ic.nitrat, ic.sulfat, ic.kalium, ic.magnesium, ic.natrium
											, ic.hydrogencarbonat, ic.calcium) IS NOT NULL
				GROUP BY concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
				HAVING count(distinct EXTRACT(YEAR FROM sampledate)) > 3
				)
		;

	 -- Qry_chemistrydata_6 -- ionbalance smaller than 5% and existing
	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
	SET quality_waterchemistry = 6
	WHERE quality_waterchemistry >=5
		AND b.DGU IN 
			(
				WITH
               latest_samp AS
                             (SELECT DISTINCT ON (guid_intake)

                                            guid_intake, boreholeno, intakeno, gcs.sampleid, gcs.sampledate
                                            FROM jupiter.grwchemsample gcs
                                            INNER JOIN jupiter.grwchemanalysis ca USING (sampleid)
              								INNER JOIN mstjupiter.inorganic_crosstab ic ON gcs.sampleid = ic.sampleid 
			  								WHERE  ic.chlorid IS NOT NULL  
			  									AND ic.nitrat IS NOT NULL
			  									AND ic.sulfat IS NOT NULL
			  									AND ic.kalium IS NOT NULL
			  									AND ic.magnesium IS NOT NULL
			  									AND ic.natrium IS NOT NULL
			  									AND ic.hydrogencarbonat IS NOT NULL
			  									AND ic.calcium IS NOT NULL
                                            ORDER BY guid_intake, gcs.sampledate DESC NULLS LAST
                             )

					SELECT concat(gw.boreholeno, '_', COALESCE(gw.intakeno))
					FROM latest_samp gw
					INNER JOIN mstjupiter.forvitgrad_ionbalance_ionbytning ion ON gw.sampleid = ion.sampleid
					WHERE abs(ion.ionbalance) < 5 AND ion.ionbalance IS NOT NULL
				)
		;
	
	
    -- CHEMISTRY QUALITY SUM
    ALTER TABLE temp_makyn.borehole_waterchemistry_quality
   	ADD COLUMN IF NOT EXISTS quality_waterchemistry_complete INTEGER DEFAULT 0,
   	ADD COLUMN IF NOT EXISTS quality_waterchemistry_complete_descr text DEFAULT 'Ingen uorganisk kemidata og det er ikke muligt at udtage prøver fra boringen.';

  	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 1,
		quality_waterchemistry_complete_descr = 'Ingen uorganisk kemidata, men det er muligt at udtage prøver fra boringen.'
    WHERE quality_waterchemistry < 2
   		AND is_demolished = 'f';

	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 2,
		quality_waterchemistry_complete_descr = 'Seneste prøve er ældre end 5 år.'
	WHERE quality_waterchemistry = 2
	;

   	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 3,
		quality_waterchemistry_complete_descr = 'Seneste prøve er ældre end 5 år, men der er flere end 3 prøver fra forskellige år.'
    WHERE quality_waterchemistry = 3
   	;

   	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 4,
		quality_waterchemistry_complete_descr = 'Seneste prøve er yngre end 5 år.'
    WHERE quality_waterchemistry = 4
   	;

  	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 5,
		quality_waterchemistry_complete_descr = 'Seneste prøve er yngre end 5 år, og der er flere end 3 prøver fra forskellige år, men ionbalancen er over 5%.'
    WHERE quality_waterchemistry = 5
    ;

   	UPDATE temp_makyn.borehole_waterchemistry_quality AS b
    SET
		quality_waterchemistry_complete = 6,
		quality_waterchemistry_complete_descr = 'Seneste prøve er yngre end 5 år, der er flere end 3 prøver fra forskellige år og ionbalancen er under 5%.'
    WHERE quality_waterchemistry = 6
    ;

GRANT USAGE ON SCHEMA temp_makyn TO grukosreader;

GRANT SELECT ON ALL TABLES IN SCHEMA temp_makyn TO grukosreader; 
 
COMMIT;
