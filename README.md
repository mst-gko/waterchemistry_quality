# Waterchemistry Quality

## Description

The chemical quality assessment of boreholes and intakes, is a SQL tool that produces an estimated quality assessment of boreholes and intakes in the Jupiter database. 

The SQL files require three existing views from mstjupiter:
1. **chemsamp_crosstab_json:** A JSON file containing information about the samples and the specific measurements.
2. **inorganic_crosstab:** A SQL View of the JSON-crosstab showing a table with the measurements from the samples.
3. **forvitgrad_ionbalance_ionbytning:** A SQL View showing a table with the calculated weathering degree, ionbalance and ion exchange of the samples.


The SQL file "quality_sample_waterchemistry" yields a quality category of groundwater chemistry samples of the specific borehole/intake.

The SQL file "waterchemistry_quality" reuses the quality category and procudes a table suitable for borehole/intake quality assessment regarding groundwater chemistry. 

![Flowchart of waterchemistry_quality (Latest sample referes to latest inorganic sample w. components for ionbalance calculations)](/flowchart/flowchart_engelskversion.png "Flowchart")


**Figure: Flowchart of waterchemistry_quality (Latest sample refers to latest inorganic sample w. components for ionbalance calculations)**

## Output
### In general
The tool produces a quality assessment of the quality, age and number of chemical samples for a given boreholeintake. The intention is established to improve the startup analysis of the groundwater chemical basis in order to outline the basis of data and the need for additional sampling. 

The quality assessment can be described as follows:
- **Category 0:** No inorganic groundwater chemistry data and it is not possible to extract samples.
- **Category 1:** No inorganic groundwater chemistry data and it is possible to extract samples
- **Category 2:** Latest sample (inorganic w. components for ionbalance calculations) is older than 5 years.
- **Category 3:** Latest sample (inorganic w. components for ionbalance calculations) is older than 5 years and more than 3 samples from different years exist.
- **Category 4:** Latest sample (inorganic w. components for ionbalance calculations) is younger than 5 years.
- **Category 5:** Latest sample (inorganic w. components for ionbalance calculations) is younger than 5 years, more than 3 samples from different years exist and the ionbalance is above 5%, unknown or not possible to calculate.
- **Category 6:** Latest sample (inorganic w. components for ionbalance calculations) is younger than 5 years, more than 3 samples from different years exist and the ionbalance is below 5%.

### Postgresql - Initial (’quality_sample_waterchemistry’)
- **boreholeid:** The actual unique identifier for boreholes in the Jupiter database.
- **boreholeno:**  The DGU number defined in the Jupiter database. Note that this is the only unique identifier for boreholes in the Jupiter database.
- **sampleid:** The unique identifier for a sample of groundwater chemistry.
- **sampledate:** Date and time of the given sampling.
- **intakeno:** The externally known identifier of the intake of the borehole. 
- **compoundno:** The specific parameter, measured for in the sample. 
- **long_text:** The long name of the specific parameter.
- **uuid:** The unique ID for the sample. 
- **is_demolished:** A boolean true (1) / false (0) regarding wether the borehole is demolished and can be found during field work.
- **geom:** The geometry of the borehole retrieved from the Jupiter database.
- **dgu:** A collection identifier of the boreholeno and intakeno.
- **quality_waterchemistry:** The initial quality assessment on the basis of the specified conditions.
- **quality_waterchemistry_complete:** The final quality assessment between 0 and 6, where 6 is of the highest quality. This exists for all samples but is linked to the specific boreholeintake. 
- **quality_waterchemistry_complete_descr:** Description of the final quality assessment. 


### Postgresql - Complete (’waterchemistry_quality’)
- **boreholeid:** The actual unique identifier for boreholes in the Jupiter database.
- **boreholeno:**  The DGU number defined in the Jupiter database. Note that this is the only unique identifier for boreholes in the Jupiter database.
- **intakeno:** The externally known identifier of the intake of the borehole. 
- **dgu:** A collection identifier of the boreholeno and intakeno.
- **geom:** The geometry of the borehole retrieved from the Jupiter database.
- **quality_waterchemistry:** The initial quality assessment on the basis of the specified conditions.
- **quality_waterchemistry_complete:** The final quality assessment between 0 and 6, where 6 is of the highest quality. This exists for all samples but is linked to the specific boreholeintake. 
- **quality_waterchemistry_complete_descr:** Description of the final quality assessment. 
- **url:** The url of the borehole report of the borehole.
- **no_of_samples:** The number of inorganic chemistry samples (w. components for ionbalance calculations) for the specific boreholeintake.
- **latest_sample_date:** The date and time of the latest inorganic chemistry sample (w. components for ionbalance calculations).
- **ionbalancen:** Ionbalance for the specific sample, if it is possible to calculate.
- **nitrat:** Measurement of nitrate from sample, if it is measured.
- **sulfat:** Measurement of sulphate from sample, if it is measured.
- **klorid:** Measurement of chloride from sample, if it is measured.
- **jern:** Measurement of iron from sample, if it is measured.
- **kalium:** Measurement of potassium from sample, if it is measured.
- **magnesium:** Measurement of magnesium from sample, if it is measured.
- **natrium:** Measurement of sodium from sample, if it is measured.
- **hydrogencarbonat:** Measurement of hydrogencarbonate from sample, if it is measured.
- **calcium:** Measurement of calcium from sample, if it is measured.

## Help
For help contact: [Martin Kynde](mailto:makyn@mst.dk)

## Author and licence
Borehole_quality is written by Martin Kynde, and is licensed under the GNU Public License version 3 or later. See [LICENSE](LICENSE) for details.
